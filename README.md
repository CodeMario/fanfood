Ionic App FanFood
=====================

## Link

https://gitlab.com/CodeMario/fanfood

## Integrantes

Mario José Hernández Sánchez 		HS111379
Gustavo Adolfo Melendez Bonilla		MB131350
Marco Antonio Rivas Roque			RR101253

## Intro

Aplicación para restaurantes y usuarios, en ella podemos encotrar la variedad de empresas gastronomicas salvadoreñas, valorar, comentar, observar eventos en tiempo real y crear un ambiente de satisfacción para el cliente y la empresa.

## Using this project

Para correr la aplicación: 

```bash
$ ionic serve
```
Para poder utilzar las actividades debe de registrarce.

## Issues
La aplicación trabaja con Firebase version Legacy Console, hasta esta fecha 10-07-2016 esta ya es depresiada.

