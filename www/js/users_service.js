(function(){
    
var app = angular.module('starter.usermodel', ['ionic','firebase']);
var urlBase = "https://fanfood.firebaseio.com";
var idUser;
  
app.factory('Usuario',function($firebaseArray,$firebaseObject,$firebase,$state){
    
    var restsFirebase = new Firebase(urlBase);
    var refUser = new Firebase(urlBase+"/usuarios");

    var idUser = "";

    return {
        list: function(){               
            return $firebaseArray(restsFirebase);
        },
        getElement: function(Id,element){
            var rest = new Firebase(refUser+"/"+Id+"/"+element);
            return $firebaseObject(rest);
        },
        get: function(Id){
            var rest = new Firebase(refUser+"/"+Id);
            return $firebaseObject(rest);
        },
        getLikes : function(Id){
            var rest = new Firebase(urlBase+"/userlike/"+Id);
            return $firebaseArray(rest);
        },
        saveLike: function(Id,Idr,Name,Img){
            var refLikes = new Firebase(urlBase+"/usuarios/"+Id+"/likes");
            var newLike = refLikes.child(Idr);
            var data = {id:Idr,nombre:Name,ranking:0};
            newLike.set(data);

            var refLikesUser = new Firebase(urlBase+"/userlike/"+Id);
            var newLikeUser = refLikesUser.push();
            var datas = {idu:Id,idr:Idr,nombre:Name,Imagen:Img};
            newLikeUser.set(datas);
        }, 
        saveMyRanking: function(Id,Idr,ranking){
            var restt = new Firebase(urlBase+"/usuarios/"+Id+"/rankings/"+Idr);
            var nranking = parseInt(ranking);
            var data = {ranking:nranking};
            restt.update(data);
        },// Guardar usuario por email y passwd
        save: function(usernam,passwd,mail){            
            restsFirebase.createUser({
              email    : mail,
              password : passwd
            }, function(error, userData) {
              if (error) {
                console.log("Error creating user:", error);
              } else {
                idUser = userData.uid;
                console.log("Successfully created user account with uid:", idUser);
                var newUser = refUser.child(idUser);
                var data = {user:usernam,email:mail,password:passwd,active:0};
                newUser.set(data);
                var data = {user:usernam,state:'success'};                
                return data;
              }
            });
        },// Guardar usuario por Facebook
        saveFacebook: function(){
            restsFirebase.authWithOAuthPopup("facebook", function(error, authData) {
                if (error) {
                    console.log("Login Failed!", error);
                } else {
                    console.log("Authenticated successfully with payload:", authData);
                }
            });
        },
        login: function(email,passwd){
            restsFirebase.authWithPassword({
                email:email,
                password:passwd
            },function(error,authData){
                if (error) {
                    return false;
                }else{
                    window.localStorage.setItem('ide',authData.uid);
                    $state.transitionTo('tab.home');
                    return true;
                }
            });
        },
        logout:function(){
            window.localStorage.removeItem('ide');
            return false;
        }
    }; 
});
    
    

}());