
(function(){
    
  var app = angular.module('starter.restaurantesmodel', ['ionic','firebase']);
  
  var urlBase = "https://fanfood.firebaseio.com/restaurantes";
  
    app.factory('Restaurantes',function($firebaseArray,$firebaseObject){
        var restsFirebase = new Firebase(urlBase);
        return {
            list: function(){               
                return $firebaseArray(restsFirebase);
            },
            getElement: function(Id,element){
                var rest = new Firebase(urlBase+"/"+Id+"/"+element);
                return $firebaseObject(rest);
            },
            get: function(Id){
                return $firebaseObject(restsFirebase.child(Id))
            },
            comment: function(usuario,mensaje,id,userid){
                var refc = new Firebase(urlBase+"/"+id+"/comentarios");
                var newComment = refc.push();
                var data = {userkey:userid,user:usuario,mensaje:mensaje};
                newComment.set(data);
            },
            updateComments: function(Id,num){
                var rest = new Firebase(urlBase+"/"+Id);
                var sumCom = parseInt(num) + 1;
                // var newComment = rest.child();
                var data = {comments:sumCom};
                rest.update(data);                  
            },
            updateLikes: function(Id,num){
                var rest = new Firebase(urlBase+"/"+Id);
                var sumCom = parseInt(num) + 1;
                var data = {likes:sumCom};
                rest.update(data);  
            }, ///// EN CONSTRUCCION
            updateRanking: function(Id,ran,nran){
                var rest = new Firebase(urlBase+"/"+Id);
                var nranking = ((parseInt(ran) + parseInt(nran))/100);
                var data = {ranking:nranking};
                rest.update(data);
            }
        }; 
    });
    
    

}());