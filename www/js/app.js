
(function(){
    

var app = angular.module('starter', ['ionic', 
                   'starter.controllers', 
                   'starter.services',
                   'starter.restaurantesmodel',
                   'starter.usermodel'])


app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {

    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);      
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
    
  });
})

app.config(function($stateProvider,$urlRouterProvider,$ionicConfigProvider) {

  $ionicConfigProvider.tabs.position('bottom'); // other values: top

  $stateProvider.state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  });

  $stateProvider.state('tab.home', {
    url: '/home',
    views: {
      'tab-home': {
        templateUrl: 'templates/tab-home.html',
        controller: 'HomeCtrl'
      }
    }
  });
  
  $stateProvider.state('tab.home-detail',{
      url:'/home/:Id',
      views: {
      'tab-home': {
          templateUrl:'templates/restaurante.html',
          controller:'HomeDetailCtrl'
          }
      }      
  });
  
  $stateProvider.state('tab.search',{
      url:'/search',      
      views:{
          'tab-search':{
              templateUrl:'templates/tab-search.html',
              controller:'SearhcCtrl'
          }
      }
  });

  $stateProvider.state('tab.search-detail',{
      url:'/search/:Id',
      views: {
      'tab-search': {
          templateUrl:'templates/restaurante.html',
          controller:'HomeDetailCtrl'
          }
      }      
  });

  $stateProvider.state('tab.likes', {
    url: '/likes',
    views: {
      'tab-likes': {
        templateUrl: 'templates/tab-likes.html',
        controller: 'FavoritesCtrl'
      }
    }
  });

  $stateProvider.state('tab.likes-detail', {
    url: '/likes/:Id',
    views: {
      'tab-likes': {
        templateUrl:'templates/restaurante.html',
        controller:'HomeDetailCtrl'
      }
    }
  });

  $stateProvider.state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    });

  $stateProvider.state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  $stateProvider.state('tab.account-new', {
    url: '/newuser',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-newuser.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // $stateProvider.state('tab.account-validate', {
  //   url: '/newuser',
  //   abstract: true,
  //   templateUrl: 'templates/tabs.html'
  // });

  $urlRouterProvider.otherwise('/tab/home');

});
    
}());