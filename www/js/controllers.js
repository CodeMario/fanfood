

(function(){
    var app=angular.module('starter.controllers', ['starter.restaurantesmodel',
                                                    'starter.usermodel']);
    
    app.controller('HomeCtrl',function($scope,$state,Restaurantes){
        $scope.restaurantes =  Restaurantes.list();
        $scope.onLike = function(){
            console.log("prueba de onClick");
        };
    })

    app.controller('HomeDetailCtrl',function($scope,$state,$document,$timeout,Restaurantes,Usuario){
      $scope.id = $state.params.Id;        
      $scope.range = 0;

      // validacion para mostrar o no el formulario de cometario
      var token = window.localStorage.getItem('ide') || "";        
      if(token.trim() === ""){
        $scope.session = false;
        $scope.state = false;
      }else{          
        $scope.session = true;
        $scope.state = true;
      }

      $scope.datauser = Usuario.getElement(token,"user");
      
      $scope.comments = Restaurantes.getElement($scope.id,"comments");
      $scope.likes = Restaurantes.getElement($scope.id,"likes");

      $scope.img = Math.floor(Math.random()* 4+1);
      $scope.restaurante = Restaurantes.get($scope.id);
      $scope.message = "";

      $scope.onComment = function(message){
        if (message.trim()!=="") {
          Restaurantes.comment($scope.datauser.$value,message,$scope.id,token);
          Restaurantes.updateComments($scope.id,$scope.comments.$value);
          console.log($scope.comments.$value);
          $scope.message = null;
        }
      }
      // Agregar a me gustan o favoritos del sisetmas
      $scope.like = Usuario.getElement(token,"likes/"+$scope.id+"/id");
      $scope.onLike = function(){
        $scope.state = Usuario.saveLike(token,$scope.id,$scope.restaurante.name,$scope.restaurante.imgProfile);
        Restaurantes.updateLikes($scope.id,$scope.likes.$value);        
      }

      $scope.ranking = Restaurantes.getElement($scope.id,"ranking");


      $scope.data = { 'volume' : 0 };

      var timeoutId = null;
      $scope.$watch('data.volume', function() {

        Usuario.saveMyRanking(token,$scope.id,$scope.data.volume);
        if ($scope.data.volume != 0) {
          Restaurantes.updateRanking($scope.id,$scope.ranking.$value,$scope.data.volume);          
        };

        if(timeoutId !== null) {
          return;
        }

        console.log('Not going to ignore this one');
        timeoutId = $timeout( function() {          
          console.log('It changed recently!');          
          $timeout.cancel(timeoutId);
          timeoutId = null;          
          // Now load data from server 
        }, 1000); 


      });


    })


    app.controller('SearhcCtrl',function($scope,Restaurantes){
      $scope.restaurantes = Restaurantes.list();
    })

    app.controller('FavoritesCtrl', function($scope, Restaurantes, Usuario) {
      var token = window.localStorage.getItem('ide') || "";        
      if(token.trim() === ""){
        $scope.session = false;
        $scope.state = false;
      }else{          
        $scope.session = true;
        $scope.state = true;
      }
      $scope.restaurantes = Usuario.getLikes(token);      
    })

    app.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
      $scope.chat = Chats.get($stateParams.chatId);
    })


    app.controller('AccountCtrl', function($scope,$rootScope,$state,Usuario) {

      // -----------
      $rootScope.dataSession = {state:'fail'};
      $scope.img = Math.floor(Math.random()* 6+1);

      var token = window.localStorage.getItem('ide') || "";
      if(token.trim() === ""){
        $scope.session =false;
      }else{
        $scope.datauser = Usuario.get(token);
        $scope.session =true;
      }

      
      // codigo para registrar usuario por email y passwd
      $scope.onSingUp = function(username,password,email){
        $rootScope.dataSession = Usuario.save(username,password,email);
      }
      
      // codigo para registrar usuario por facebook
      $scope.onSingUpFacebook = function(){
        Usuario.saveFacebook();
      }
      
      // codigo para iniciar sesion por email y passwd
      $scope.onSingIn = function(email,password){
        $scope.session = estado = Usuario.login(email,password);        
      }

      // codigo para cerrar la sesion
      $scope.onSingOut = function(){
        $scope.session = Usuario.logout();
      }


      


    });

}());
